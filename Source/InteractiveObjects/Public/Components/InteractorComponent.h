// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Interfaces/InteractionTemplate.h"
#include "InteractorComponent.generated.h"

class AInteractableActor;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class INTERACTIVEOBJECTS_API UInteractorComponent : public UActorComponent, public IInteractionTemplate
{
	GENERATED_BODY()

private:
	/** Array of all interactable actors that have been registered */
	UPROPERTY()
		TArray<AInteractableActor*> Interactables;

	/** Focused Interactable actor that is registered */
	UPROPERTY(BlueprintReadWrite, Category = "InetarctionSystem", meta = (AllowPrivateAccess = "true"))
		AInteractableActor* FocusedInteractable;

	/** Register Interactable Object and add in Interactables array */
	void HandleRegisterInteractable(class AInteractableActor* Interactable);

	/** Remove the interactable from array and unregister Interactable object */
	void HandleUnregisterInteractable(class AInteractableActor* Interactable);

	/** Focus registered interactable in sight of dot product value */
	void SetBestInteractable(class AInteractableActor* Interactable);

	/** Calculates best interactable to focus if there is one*/
	void GetBestInteractable();

	/** Check if we have interactable or what to focus if found more than 1 */
	void UpdateBestInteractable();
	
public:	
	// Sets default values for this component's properties
	UInteractorComponent();
	
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void TryInteract();

	/** Interface to register focused interactable object */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InteractionTemplate")
		void RegisterFocus(class AActor* Interactable);
	virtual void RegisterFocus_Implementation(class AActor* Interactable) override;

	/** Interface to register focused interactable object */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InteractionTemplate")
		void UnregisterFocus(class AActor* Interactable);
	virtual void UnregisterFocus_Implementation(class AActor* Interactable) override;

	/** Interface for interacting with interactable objects */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InteractionTemplate")
		void Interact(class AActor* _Instigator);
	virtual void Interact_Implementation(class AActor* Causer) override;
};
