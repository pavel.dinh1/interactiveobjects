#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/InteractionTemplate.h"
#include "InteractableActor.generated.h"

USTRUCT(BlueprintType)
struct FInteractableStruct
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Category = "InteractableStruct")
		float DotProduct;

	UPROPERTY(BlueprintReadWrite, Category = "InteractableStruct")
		class AInteractableActor* Interactable;

	FInteractableStruct()
	{
		DotProduct = -1.f;
		Interactable = nullptr;
	}
};

UCLASS()
class INTERACTIVEOBJECTS_API AInteractableActor : public AActor, public IInteractionTemplate
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AInteractableActor();

protected:
	// Visual representation of the actor
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UStaticMeshComponent* InteractableMesh;

	// Collision to detect if the player is in the zone
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class USphereComponent* InteractableZone;

	UPROPERTY(EditAnywhere, Category = "Rendering", meta = (ClampMin = "0", ClampMax = "6"))
		int32 StencilValue;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Event function to register interactable actor
	UFUNCTION()
		void RegisterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Event function to unregister interactable actor
	UFUNCTION()
		void UnregisterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:
	// Get mesh from interactable actor
	FORCEINLINE UStaticMeshComponent* GetInteractableMesh() const
	{
		return InteractableMesh;
	}

	// Highlight the interactable mesh
	UFUNCTION()
		void SetFocused(bool bShouldFocus) const;

	/** Interface to register focused interactable object */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InteractionTemplate")
		void RegisterFocus(class AActor* Interactable);
	virtual void RegisterFocus_Implementation(class AActor* Interactable) override;

	/** Interface to register focused interactable object */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InteractionTemplate")
		void UnregisterFocus(class AActor* Interactable);
	virtual void UnregisterFocus_Implementation(class AActor* Interactable) override;

	/** Interface for interacting with interactable objects */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InteractionTemplate")
		void Interact(class AActor* Causer);
	virtual void Interact_Implementation(class AActor* Causer) override;
};
