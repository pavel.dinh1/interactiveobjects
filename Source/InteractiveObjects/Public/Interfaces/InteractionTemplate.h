#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InteractionTemplate.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractionTemplate : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class INTERACTIVEOBJECTS_API IInteractionTemplate
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InteractionTemplate")
		void RegisterFocus(class AActor* Interactable);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InteractionTemplate")
		void UnregisterFocus(class AActor* Interactable);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InteractionTemplate")
		void Interact(class AActor* Causer);
};
