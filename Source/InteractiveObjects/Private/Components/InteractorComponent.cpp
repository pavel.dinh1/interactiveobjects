// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/InteractorComponent.h"
#include "Interactors/InteractableActor.h"
#include "DrawDebugHelpers.h"

#define LOG(x) UE_LOG(LogTemp, Warning, TEXT(x))

// Sets default values for this component's properties
UInteractorComponent::UInteractorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	FocusedInteractable = nullptr;
}

void UInteractorComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateBestInteractable();
}

void UInteractorComponent::RegisterFocus_Implementation(AActor* Interactable)
{
	if (AInteractableActor* InteractableActor = Cast<AInteractableActor>(Interactable))
	{
		HandleRegisterInteractable(InteractableActor);
	}
}

void UInteractorComponent::UnregisterFocus_Implementation(AActor* Interactable)
{
	if (AInteractableActor* InteractableActor = Cast<AInteractableActor>(Interactable))
	{
		HandleUnregisterInteractable(InteractableActor);
	}
}

void UInteractorComponent::Interact_Implementation(AActor* Causer)
{
	if (FocusedInteractable)
	{
		FocusedInteractable->Execute_Interact(FocusedInteractable, GetOwner());
	}
}

void UInteractorComponent::TryInteract()
{
	IInteractionTemplate::Execute_Interact(this, nullptr);
}

void UInteractorComponent::HandleRegisterInteractable(AInteractableActor* Interactable)
{
	Interactables.AddUnique(Interactable);
}

void UInteractorComponent::HandleUnregisterInteractable(AInteractableActor* Interactable)
{
	Interactable->SetFocused(false);
	Interactables.Remove(Interactable);
	if (FocusedInteractable)
	{
		FocusedInteractable->SetFocused(false);
		FocusedInteractable = nullptr;
	}
}

void UInteractorComponent::SetBestInteractable(AInteractableActor* Interactable)
{
	if (!Interactable) { return; }
	if (Interactable == FocusedInteractable) { return; }

	if (!FocusedInteractable)
	{
		FocusedInteractable = Interactable;
		FocusedInteractable->SetFocused(true);
		return;
	}

	FocusedInteractable->SetFocused(false);
	FocusedInteractable = Interactable;
	FocusedInteractable->SetFocused(true);
}

void UInteractorComponent::GetBestInteractable()
{
	FInteractableStruct InteractableInfo;

	for (AInteractableActor* Interactable : Interactables)
	{
		FVector InteractableCenterLocation = Interactable->GetInteractableMesh()->GetCenterOfMass("None");
		FVector CameraLocation;
		FRotator CameraRotation;

		GetOwner()->GetActorEyesViewPoint(CameraLocation, CameraRotation);

		FVector DistanceView = (InteractableCenterLocation - CameraLocation).GetSafeNormal();
		float LocalDotProduct = FVector::DotProduct(DistanceView, CameraRotation.Vector());

		if (LocalDotProduct > 0.3f && LocalDotProduct > InteractableInfo.DotProduct)
		{
			InteractableInfo.DotProduct = LocalDotProduct;
			InteractableInfo.Interactable = Interactable;
		}
	}

	(InteractableInfo.Interactable) ? SetBestInteractable(InteractableInfo.Interactable) : SetBestInteractable(nullptr);
}

void UInteractorComponent::UpdateBestInteractable()
{
	(Interactables.Num() > 0) ? GetBestInteractable() : SetBestInteractable(nullptr);
}