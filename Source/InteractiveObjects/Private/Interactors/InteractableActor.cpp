#include "Interactors/InteractableActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/InteractorComponent.h"

// Sets default values
AInteractableActor::AInteractableActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	InteractableMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("InteractableMesh"));
	RootComponent = InteractableMesh;

	InteractableZone = CreateDefaultSubobject<USphereComponent>(TEXT("InteractableZone"));
	InteractableZone->SetupAttachment(InteractableMesh);
	InteractableZone->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;

	StencilValue = 1;
}

// Called when the game starts or when spawned
void AInteractableActor::BeginPlay()
{
	Super::BeginPlay();

	InteractableZone->OnComponentBeginOverlap.AddDynamic(this, &AInteractableActor::RegisterOverlap);
	InteractableZone->OnComponentEndOverlap.AddDynamic(this, &AInteractableActor::UnregisterOverlap);
}

void AInteractableActor::SetFocused(bool bShouldFocus) const
{
	GetInteractableMesh()->SetRenderCustomDepth(bShouldFocus);
	GetInteractableMesh()->SetCustomDepthStencilValue(StencilValue);
}

void AInteractableActor::RegisterFocus_Implementation(AActor* Interactable)
{
	// This interactor doesn't need to register itself
}

void AInteractableActor::UnregisterFocus_Implementation(AActor* Interactable)
{
	// This interactor doesn't need to unregister itself
}

void AInteractableActor::Interact_Implementation(class AActor* Causer)
{
	UE_LOG(LogTemp, Warning, TEXT("Actor %s has been Interacted !"), *this->GetName());
}

void AInteractableActor::RegisterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (auto TargetComp = OtherActor->FindComponentByClass<UInteractorComponent>())
	{
		IInteractionTemplate::Execute_RegisterFocus(TargetComp, this);
	}
}

void AInteractableActor::UnregisterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (auto TargetComp = OtherActor->FindComponentByClass<UInteractorComponent>())
	{
		IInteractionTemplate::Execute_UnregisterFocus(TargetComp, this);
	}
}