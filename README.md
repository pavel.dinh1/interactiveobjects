# UE4 Interactive Objects

* Unreal engine 4 custom plugin for interaction with environment. Pickups, Door opening, light triggers etc.  

## Current features:
* Pickup object

## Technical details
* UE4 version: 4.25
* IDE: Visual Studio 2019 Community